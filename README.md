# cesiumBundle

> ## cesium 封装
>
> ## 打包使用 rollup,采用原型方法的方式集成功能

> ## 目录说明

src 框架主要目录  
|--func/ 功能  
|--untl/ 工具  
|--index.js 入口文件  
|--mixin.js 功能与工具装配

---

public 公共资源  
|--cesiumSource cesium 静态资源  
|--img  
|--js

---

dist rollup 打包后的包  
|--cesiumBundle.js 打包整合后的压缩 js 文件  
|--cesiumBundle.js.map 代码调试 sourceMap

> ## cesium 包修改 token

```
cesium/core/lon.js --> defaultAccessToken
```

> ## 使用 `<script type="module" src="./index.js"></script>`

重新编译库,一般需要 8-9s.  
使用浏览器自带的 module 模式,做到真正的按需引入(vite 万岁!)  
你需要在你的 js 文件里用 ES6 的方式,引入库的入口文件(编译前的)
