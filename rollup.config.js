import babel from "rollup-plugin-babel";
// import serve from "rollup-plugin-serve";

export default {
  input: "./src/index.js",
  output: {
    file: "dist/umd/cesiumBundle.js",
    name: "CB", //打包后全局变量名字
    format: "umd", //打包的模块规范
    sourcemap: true, //源码调试
  },
  plugins: [
    babel({
      exclude: "node_modules/**", //排除目录
    }),
    //环境变量为开发模式时,启动serve
    // process.env.ENV === "development"
    //   ? serve({
    //       open: true,
    //       openPage: "/public/index.html", //默认打开的html路径
    //       port: 3000,
    //       contentBase: "", //静态文件目录
    //     })
    //   : null,
  ],
};
