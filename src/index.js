import init from "./func/init.js";
import mixin from "./mixin.js";
function CB() { }

//初始化,不要放到mixin里去
init(CB);
//在原型上装配各种功能
mixin(CB);

export default CB;

