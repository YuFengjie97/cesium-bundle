/**
 * 装配func
 */
import { createModel } from "./func/createModel.js";
import { zone } from "./func/zone.js";
import { showCardByClickEntity } from "./func/showCardByClickEntity.js";
import measureLine from './func/measureLine.js'
import measureArea from './func/measureArea.js'

/**
 * 装配util
 */
import { fromDegreesArrayHeights } from "./util/dataTransform.js";
import { getLonAndLatByClick } from "./util/getLonAndLatByClick.js";
import { createBluePin } from "./func/createBluePin.js";

let funcs = {
  createModel,
  zone,
  showCardByClickEntity,
  measureLine,
  measureArea,

  fromDegreesArrayHeights,
  getLonAndLatByClick,
  createBluePin,

};

//在原型上自动循环装配
export default function mixin(CB) {
  // 去循环遍历funcs,把他们通过原型的方式,装配到CB上
  for (let key in funcs) {
    CB.prototype[key] = funcs[key];
  }
}
