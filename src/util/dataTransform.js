export function fromDegreesArrayHeights(array) {
  return this.Cesium.Cartesian3.fromDegreesArrayHeights(array);
}
