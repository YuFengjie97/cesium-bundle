export function getLonAndLatByClick() {
  let Cesium = this.Cesium;
  let viewer = this.viewer;

  //鼠标点击获取经纬度
  var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);

  handler.setInputAction(function (event) {
    var earthPosition = viewer.camera.pickEllipsoid(
      event.position,
      viewer.scene.globe.ellipsoid
    );
    var cartographic = Cesium.Cartographic.fromCartesian(
      earthPosition,
      viewer.scene.globe.ellipsoid,
      new Cesium.Cartographic()
    );
    var lat = Cesium.Math.toDegrees(cartographic.latitude);
    var lng = Cesium.Math.toDegrees(cartographic.longitude);
    var height = cartographic.height;
    console.log("[Lon=>" + lng + ",Lat=>" + lat + ",H=>" + height + "]");
    let posi = { x: lng, y: lat };
  }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
}
