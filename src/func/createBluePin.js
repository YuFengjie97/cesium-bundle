/**
 *
 * @param {*} name
 * @param {*} id
 * @param {*} posi
 */
export function createBluePin(name, id, posi) {
  let Cesium = this.Cesium;
  let viewer = this.viewer;

  let pinBuilder = new Cesium.PinBuilder();

  let entity = viewer.entities.add({
    name: name,
    id: id,
    done: false, //entities已添加标志
    position: Cesium.Cartesian3.fromDegrees(posi.lon, posi.lat),
    billboard: {
      image: pinBuilder.fromColor(Cesium.Color.ROYALBLUE, 48).toDataURL(),
      verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
    },
  });
  return entity;
}
