/**
 *
 * @param {*} lon 左下角经度
 * @param {*} lat 左下角纬度
 * @param {*} x 长,经度距离//搞笑
 * @param {*} y 宽,纬度距离//搞笑
 * @param {*} h 高
 */
export function zone({ lon, lat, x, y, h }) {
  let Cesium = this.Cesium;
  let viewer = this.viewer;

  let info = [
    lon,
    lat,
    h,
    lon + x,
    lat,
    h,
    lon + x,
    lat + y,
    h,
    lon,
    lat + y,
    h,
    lon,
    lat,
    h,
  ];
  let positions = this.fromDegreesArrayHeights(info);

  var alp = 1;
  var num = 0;
  //绘制墙
  var entity = viewer.entities.add({
    name: "动态立体墙",
    wall: {
      show: true,
      positions: positions,
      material: new Cesium.ImageMaterialProperty({
        image: "../../../public/img/bg.png", //这个路径应该是以打包后的js为参照,去查找img,不能以当前的js为参考
        transparent: true,
        color: new Cesium.CallbackProperty(function () {
          if (num % 2 === 0) {
            alp -= 0.005;
          } else {
            alp += 0.005;
          }

          if (alp <= 0.3) {
            num++;
          } else if (alp >= 1) {
            num++;
          }
          return Cesium.Color.WHITE.withAlpha(alp);
          //entity的颜色透明 并不影响材质，并且 entity也会透明
        }, false),
      }),
    },
  });
  return entity;
}
