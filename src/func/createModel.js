/**
 *
 * @param {*} url glb模型文件路径
 * @param {*} posi
 */
export function createModel(url, posi) {
  let Cesium = this.Cesium;
  let viewer = this.viewer;

  //坐标转换
  let position = Cesium.Cartesian3.fromDegrees(posi.x, posi.y, posi.height);

  //定义欧拉角
  var heading = Cesium.Math.toRadians(0);
  var pitch = 0;
  var roll = 0;
  var hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
  var orientation = Cesium.Transforms.headingPitchRollQuaternion(position, hpr);

  //创建实体
  //通过init里绑定在prototype上的viewer创建entity
  var entity = viewer.entities.add({
    name: "飞机",
    position: position,
    orientation: orientation,
    model: {
      uri: url,
      minimumPixelSize: 128,
      maximumScale: 20000,
    },
  });

  return entity;
}
