export function showCardByClickEntity() {
  let Cesium = this.Cesium;
  let viewer = this.viewer;

  var scene = viewer.scene;
  var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
  //双击鼠标,清除默认事件
  viewer.cesiumWidget.screenSpaceEventHandler.removeInputAction(
    Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK
  );

  //实体的全局变量,存储已init的全部entity
  let entities = [];

  //点击实体时,获取实体,并绘制弹框
  handler.setInputAction(function (click) {
    // console.log("scene信息:", scene);

    var pick = scene.pick(click.position);
    //从pick里获取当前点击的entity
    // console.log("pick信息:", pick);//点击空白处,肯定报错啊,应为pick的id只有点击entity才有
    let entity = null;
    //没用,点击空白就会报错
    // if (pick.hasOwnProperty("id")) {
    //   entity = pick.id;
    // }
    // if (typeof pick.id != "undefined") {
    //   entity = pick.id;
    // }
    entity = pick.id;
    if (pick && pick.id && pick.id._done == false) {
      //添加后,设置为已添加,防止下次点击,重复添加
      entity._done = true;
      initMessageBox(entity);
      entities.push(entity);
    }
    //初始化后设置显示
    let id = entity._id;
    $("#" + id).css("visibility", "visible");
  }, Cesium.ScreenSpaceEventType.LEFT_CLICK);

  //实时渲染=(鼠标移动+滚轮缩放+滚轮中键)
  viewer.scene.postRender.addEventListener(function () {
    if (entities.length != 0) {
      updateMessageBox(entities);
    }
  });

  //监听弹框关闭
  $("body").on("click", ".exitBox", function () {
    $(this).parent().css("visibility", "hidden");
  });

  //传入entity,获取entity相关信息绘制messageBox
  function initMessageBox(entity) {
    let id = entity._id;
    //console.log('实体的所有信息', entity);

    //从实体信息里获取笛卡尔3D坐标
    let cartesian3 = entity._position._value;
    //console.log('实体笛卡尔3D坐标:', cartesian3);

    //3D坐标转2D坐标,用于messageBox在屏幕定位
    var cartesian2 = Cesium.SceneTransforms.wgs84ToWindowCoordinates(
      scene,
      cartesian3
    );
    //console.log(cartesian2);

    //测试用,获取实体名字
    let name = entity._name;
    //console.log(name);
    let left = cartesian2.x;
    let top = cartesian2.y;
    let messageBox = `<div
                  class="messageBox"
                  id="${id}"
                  style="
                    position: absolute;
                    top: ${top}px;
                    left: ${left}px;
                    width: 200px;
                    border-radius: 5px;
                    border:2px solid #e74c3c;
                    background-color: #3498db;
                    padding: 10px;
                    transform: translate3d(0px,0px,0px);
                  "
                >
                  entity名字:${name},
                  entityid:${id}
                  <div
                  class="exitBox"
                  style="width: 20px; height: 20px; position: absolute; top: 0; right: 0;"
                >
                  <div
                    class="exit"
                    style="
                      width: 20px;
                      height: 20px;
                      background-color: black;
                      cursor: pointer;
                    "
                  ></div>
                </div>
                </div>`;

    //将定义好的弹框,添加到容器里
    $("#cesiumContainer").append(messageBox);
  }

  //传入entity,获取entity相关信息更新messageBox
  function updateMessageBox(entities) {
    // 是用绝对定位来设置显示位置
    entities.forEach((entity) => {
      let id = entity._id;
      //console.log(id);
      let cartesian3 = entity._position._value;
      // console.log('获取到的笛卡尔3', cartesian3);
      var cartesian2 = Cesium.SceneTransforms.wgs84ToWindowCoordinates(
        scene,
        cartesian3
      );
      // console.log('转换后的笛卡尔2', cartesian2);

      let left = Math.round(cartesian2.x);
      let top = Math.round(cartesian2.y);
      //console.log({ left, top });
      $("#" + id).css("top", top);
      $("#" + id).css("left", left);
    });
  }
}
