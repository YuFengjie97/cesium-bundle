var particleSystem = viewer.scene.primitives.add(new Cesium.ParticleSystem({
    // Particle appearance
    image: '../../SampleData/fire.png',
    imageSize: new Cesium.Cartesian2(20, 20);
    startScale: 1.0,
    endScale: 4.0,
    // Particle behavior
    particleLife: 1.0,
    speed: 5.0,
    // Emitter parameters
    emitter: new Cesium.CircleEmitter(0.5),
    emissionRate: 5.0,
    emitterModelMatrix: computeEmitterModelMatrix(),
    // Particle system parameters
    modelMatrix: computeModelMatrix(),
    lifetime: 16.0
}));
