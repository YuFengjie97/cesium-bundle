import CB from './src/index.js'
//初始化---------------------------------------------------------------------
let cb = new CB();
cb.init("cesiumContainer");
let cesium = cb.Cesium
let viewer = cb.viewer
console.log('初始化后检查输出:Cesium', cesium);
console.log('初始化后检查输出:viewer', viewer);

//经纬度,高度
let posi = {
    x: 116.46,
    y: 39.92,
    height: 1000,
};
//模型加载---------------------------------------------------------------------
let url = "./public/cesiumSource/glb/Cesium_Air.glb";
//在北京上空创建一架飞机
var entity = cb.createModel(url, posi);

//视角切到创建的实体
// cb.viewer.trackedEntity = entity;

//装卸区---------------------------------------------------------------------
let zoneInfo = {
    lon: 116.38635131726113,
    lat: 39.91191082782316,
    x: 0.00909416166369,
    y: 0.00918350398439,
    h: 100,
};
var entity2 = cb.zone(zoneInfo);
cb.viewer.zoomTo(entity2); //相机到entity的位置

//点击鼠标,控制台输出经纬度,不用时,请屏蔽
// cb.getLonAndLatByClick();

//创建图钉----------------------------------------------------------------------
let bluePinPosi1 = {
    lon: 116.39089175203226,
    lat: 39.91510721386724,
};
let bluePinPosi2 = {
    lon: 116.38637079135445,
    lat: 39.921239568363106,
};
let bluePinPosi3 = {
    lon: 116.39540871872734,
    lat: 39.9211089829265,
};

let bluePin1 = cb.createBluePin("蓝色图钉1", "bluePin001", bluePinPosi1);
let bluePin2 = cb.createBluePin("蓝色图钉2", "bluePin002", bluePinPosi2);
let bluePin3 = cb.createBluePin("蓝色图钉2", "bluePin003", bluePinPosi3);



//地图圈地测面积--------------------------------------------------------------
$('#b1').on('click', function () {
    console.log('b1');
    cb.measureArea()
})
//地图画线测距--------------------------------------------------------------
$('#b2').click(function () {
    cb.measureLine()
})
////点击实体时,显示信息卡片,不用时,屏蔽--------------------------------------------------------------
$('#b3').click(function () {
    cb.showCardByClickEntity()
})